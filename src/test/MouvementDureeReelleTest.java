package test;

import avion.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.GregorianCalendar;

public class MouvementDureeReelleTest {
    private Aeroport aero1;
    private Aeroport aero2;
    private Vol vol;
    private TypeRetard av1;
    private TypeRetard av2;
    private TypeRetard ev1;
    private TypeRetard ap1;
    private TypeRetard ap2;
    private Retard retard1;
    private Retard retard2;
    private Retard retard3;
    private Retard retard4;
    private Retard retard5;
    private Retard retard6;
    private GregorianCalendar d1;
    private GregorianCalendar d2;
    private Mouvement mouv;

    @BeforeEach
    void SetUp() {
        aero1 = new Aeroport("aero", "0.12", "1.54");
        aero2 = new Aeroport("port", "0.51", "8.54");
        vol = new Vol(aero1, aero2, 188);

        av1 = new TypeRetard("av1", "pilote");
        av2 = new TypeRetard("av2", "passagers");
        ev1 = new TypeRetard("ev1", "conditions atmosphériques");
        ap1 = new TypeRetard("ap1", "conditions atmosphériques");
        ap2 = new TypeRetard("ap2", "passagers");

        retard1 = new Retard("panne de réveil", 124, true, av1);
        retard2 = new Retard("15 passagers absents : recherche de leurs bagages", 450, false, av2);
        retard3 = new Retard("panne de motivation", 23, true, av1);
        retard4 = new Retard("ouragan", 23, false, ev1);
        retard5 = new Retard("piste encombrée", 451, true, ap1);
        retard6 = new Retard("terrorisme", 464813, false, ap2);

        d1 = new GregorianCalendar(2022, 01, 12);
        d2 = new GregorianCalendar(2022, 02, 13);

        mouv = new Mouvement("DE12", vol, d1, d2);
    }

    @Test
    @DisplayName("Aucun retard")
    public void dureeReelleAucunRetard() {
        assertEquals(188,mouv.dureeReelle(), "La duree réelle doit être équivalente à la durée du vol");
    }

    @Test
    @DisplayName("un seul retard")
    public void dureeReelleUnRetard() {
        mouv.ajouterRetard(retard1);
        assertEquals(188+124,mouv.dureeReelle(), "La duree réelle doit être équivalente à la durée du vol");
    }

    @Test
    @DisplayName("plusieurs retards")
    public void dureeReellePlusieursRetards() {
        mouv.ajouterRetard(retard1);
        mouv.ajouterRetard(retard2);
        mouv.ajouterRetard(retard3);
        assertEquals(188+124+450+23,mouv.dureeReelle(), "La duree réelle doit être équivalente à la durée du vol");
    }

}
