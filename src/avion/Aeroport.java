package avion;

public class Aeroport {
    private String nom;
    private String latitude;
    private String longitude;

    public Aeroport(String nom, String latitude, String longitude){
        this.nom = nom;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
