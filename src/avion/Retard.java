package avion;

public class Retard {
    private String id;
    private String commentaire;
    private int duree;
    private boolean impliqueAeroport;
    private TypeRetard typeRetard;

    public Retard (String commentaire, int duree, boolean impliqueAeroport, TypeRetard typeRetard){
        this.commentaire = commentaire;
        this.duree = duree;
        this.impliqueAeroport = impliqueAeroport;
        this.typeRetard = typeRetard;
    }

    public Retard (String id, String commentaire, int duree, boolean impliqueAeroport, TypeRetard typeRetard){
        this(commentaire, duree, impliqueAeroport, typeRetard);
        this.id = id;
    }

    public int getDuree(){
        return duree;
    }

    public boolean isImpliqueAeroport() {
        return this.impliqueAeroport;
    }

    public TypeRetard getTypeRetard() {
        return this.typeRetard;
    }
}
