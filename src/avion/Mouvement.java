package avion;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class Mouvement {
    private String id;
    private Vol vol;
    private List<Retard> lesRetards;
    private GregorianCalendar dateDepart;
    private GregorianCalendar dateArrivee;

    public Mouvement(String id, Vol vol, GregorianCalendar dateDepart, GregorianCalendar dateArrivee){
        this.id = id;
        this.vol = vol;
        this.dateDepart = dateDepart;
        this.dateArrivee = dateArrivee;
        this.lesRetards = new ArrayList<>();
    }

    public void ajouterRetard(Retard retard){
        this.lesRetards.add(retard);
    }

    public int dureeReelle(){
        int duree = this.vol.getDuree();
        for (Retard reta : lesRetards){
            duree += reta.getDuree();
        }
        return duree;
    }

    public int dureeRetardResponsable(String typeRetard) {
        // TODO
        return 0;
    }
}
