package avion;

public class Vol {
    private int duree;
    private Aeroport aeroDepart;
    private Aeroport aeroArrivee;

    public Vol(Aeroport aeroDepart, Aeroport aeroArrivee, int duree){
        this.duree = duree;
        this.aeroDepart = aeroDepart;
        this.aeroArrivee = aeroArrivee;
    }

    public int getDuree(){
        return duree;
    }
}
