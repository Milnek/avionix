package test;

import avion.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.GregorianCalendar;

public class MouvementDureeRetardResponsableTest {
    private Aeroport aero1;
    private Aeroport aero2;
    private Vol vol;
    private TypeRetard av;
    private TypeRetard ev;
    private TypeRetard ap;
    private Retard retard1;
    private Retard retard2;
    private Retard retard3;
    private Retard retard4;
    private Retard retard5;
    private Retard retard6;
    private GregorianCalendar d1;
    private GregorianCalendar d2;
    private Mouvement mouv;

    @BeforeEach
    void SetUp() {
        aero1 = new Aeroport("aero", "0.12", "1.54");
        aero2 = new Aeroport("port", "0.51", "8.54");
        vol = new Vol(aero1, aero2, 188);

        av = new TypeRetard("av", "avant le vol");
        ev = new TypeRetard("ev", "en vol");
        ap = new TypeRetard("ap", "après le vol");

        retard1 = new Retard("panne de réveil", 124, true, av);
        retard2 = new Retard("15 passagers absents : recherche de leurs bagages", 450, false, av);
        retard3 = new Retard("panne de motivation", 23, true, av);
        retard4 = new Retard("ouragan", 23, false, ev);
        retard5 = new Retard("piste encombrée", 451, true, ap);
        retard6 = new Retard("terrorisme", 464813, false, ap);

        d1 = new GregorianCalendar(2022, 01, 12);
        d2 = new GregorianCalendar(2022, 02, 13);

        mouv = new Mouvement("DE12", vol, d1, d2);
    }

    @Test
    @DisplayName("Aucun retard")
    public void aucunRetard() {
        assertEquals(0,mouv.dureeRetardResponsable(av.getLibelle()), "la durée des retards dont l'aéroport de départ est responsable doit être nulle");
    }

    @Test
    @DisplayName("un retard d'un autre critère")
    public void unRetardNonCritereResponsable() {
        mouv.ajouterRetard(retard5);
        assertEquals(0,mouv.dureeRetardResponsable(av.getLibelle()), "la durée des retards dont l'aéroport de départ est responsable doit être nulle");
    }

    @Test
    @DisplayName("un retard en vol")
    public void unRetardEnVol() {
        mouv.ajouterRetard(retard4);
        assertEquals(0,mouv.dureeRetardResponsable(av.getLibelle()), "la durée des retards dont l'aéroport de départ est responsable doit être nulle");
    }

    @Test
    @DisplayName("un retard du bon critère non responsable")
    public void unRetardBonCritereNonResponsable() {
        mouv.ajouterRetard(retard2);
        assertEquals(0,mouv.dureeRetardResponsable(av.getLibelle()), "la durée des retards dont l'aéroport de départ est responsable doit être nulle");
    }

    @Test
    @DisplayName("un retard du bon critère responsable")
    public void unSeulRetardBonCritereResponsable() {
        mouv.ajouterRetard(retard1);
        assertEquals(124,mouv.dureeRetardResponsable(av.getLibelle()), "la durée des retards dont l'aéroport de départ est responsable doit être équivalente à la durée du retard");
    }

    @Test
    @DisplayName("plsRetardsUnSeulCorrespondant")
    public void plsRetardsUnSeulCorrespondant() {
        mouv.ajouterRetard(retard1);
        mouv.ajouterRetard(retard2);
        mouv.ajouterRetard(retard4);
        mouv.ajouterRetard(retard5);
        mouv.ajouterRetard(retard6);
        assertEquals(124,mouv.dureeRetardResponsable(av.getLibelle()), "la durée des retards dont l'aéroport de départ est responsable doit être équivalente à la durée du retard");
    }

    @Test
    @DisplayName("plsRetardsPlusieurslCorrespondant")
    public void plsRetardsPlusieurslCorrespondant() {
        mouv.ajouterRetard(retard1);
        mouv.ajouterRetard(retard2);
        mouv.ajouterRetard(retard3);
        mouv.ajouterRetard(retard4);
        mouv.ajouterRetard(retard5);
        mouv.ajouterRetard(retard6);
        assertEquals(124+23,mouv.dureeRetardResponsable(av.getLibelle()), "la durée des retards dont l'aéroport de départ est responsable doit être équivalente à la durée du retard");
    }

    @Test
    @DisplayName("queDesRetardsCorrespondant")
    public void queDesRetardsCorrespondant() {
        mouv.ajouterRetard(retard1);
        mouv.ajouterRetard(retard3);
        assertEquals(124+23,mouv.dureeRetardResponsable(av.getLibelle()), "la durée des retards dont l'aéroport de départ est responsable doit être équivalente à la durée du retard");
    }
}

