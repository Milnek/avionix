package avion;

public class TypeRetard {
    private String codeSituation;
    private String libelle;

    public TypeRetard(String codeSituation, String libelle){
        this.codeSituation = codeSituation;
        this.libelle = libelle;
    }

    public String getCodeSituation(){
        return codeSituation;
    }

    public String getLibelle() {
        return libelle;
    }
}
